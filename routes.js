var cinemasController = require('./app/controllers/cinemasController');
var moviesController = require('./app/controllers/moviesController');

module.exports = function routes(app) {
  app.get('/cinemas', cinemasController.index);
  app.post('/cinemas', cinemasController.create);
  app.post('/cinemas/add', cinemasController.create);
  app.get('/cinemas/:id', cinemasController.get);
  app.put('/cinemas/:id', cinemasController.update);
  app.put('/cinemas/:id/edit', cinemasController.update);

  app.get('/movies', moviesController.index);
  app.post('/movies', moviesController.create);
  app.post('/movies/add', moviesController.create);
  app.get('/movies/:id', moviesController.get);
  app.put('/movies/:id', moviesController.update);
  app.put('/movies/:id/edit', moviesController.update);
};