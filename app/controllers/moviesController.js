var Shared = require('./shared');
var mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;
var Movie = mongoose.model('Movie');

module.exports = {
  index: function (request, response){
    Movie.find({})
    .populate('cinemaId')
    .exec(function(error, movies){
      if(error){
        return Shared.respondMongoError(response);
      }
      response.status(200).json(movies);
    });
  },

  create: function(request, response){
    var params = request.body;
    if(!params.name){
      return response.status(400).json({
        message: 'Missing name or cinemaId'
      });
    }

    var movie =  new Movie(params);
    movie.save(function(error){
      if(error){
        return Shared.respondMongoError(response);
      }
      response.status(200).json(movie);
    });
  },

  get: function(request, response){
    var id = new ObjectId(request.params.id);
    Movie.findById(id)
    .populate('cinemaId')
    .exec(function(error, movie){
      if(error){
        return Shared.respondMongoError(response);
      }

      if(!movie){
        return response.sendStatus(404);
      }
      response.status(200).json(movie);
    });
  },

  update: function(request, response){
    var id = new ObjectId(request.params.id);
    var params = request.body;
    Movie.findByIdAndUpdate(id, params, {new: true}, function(error, movie){
      if(error){
        return Shared.respondMongoError(response);
      }

      if(!movie){
        return response.sendStatus(404);
      }

      response.status(200).json(movie);
    });
  }
};