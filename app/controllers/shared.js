module.exports = {
  respondMongoError: function(response){
    response.status(500).json({message: 'Mongo error'});
  }
};