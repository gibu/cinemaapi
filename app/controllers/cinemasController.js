var Shared = require('./shared');
var mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;
var Cinema = mongoose.model('Cinema');

module.exports = {
  index: function (request, response){
    Cinema.find({}, function(error, cinemas){
      if(error){
        return Shared.respondMongoError(response);
      }
      response.status(200).json(cinemas);
    });
  },

  create: function(request, response){
    var params = request.body;
    if(!(params.name && params.suburb)){
      return response.status(400).json({
        message: 'Missing name or suburb'
      });
    }

    var cinema =  new Cinema(params);
    cinema.save(function(error){
      if(error){
        return Shared.respondMongoError(response);
      }
      response.status(200).json(cinema.toJSON());
    });
  },

  get: function(request, response){
    var id = new ObjectId(request.params.id);
    Cinema.findById(id, function(error, cinema){
      if(error){
        return Shared.respondMongoError(response);
      }

      if(!cinema){
        return response.sendStatus(404);
      }
      response.status(200).json(cinema);
    });
  },

  update: function(request, response){
    var id = new ObjectId(request.params.id);
    var params = request.body;
    Cinema.findByIdAndUpdate(id, params, {new: true}, function(error, cinema){
      if(error){
        return Shared.respondMongoError(response);
      }

      if(!cinema){
        return response.sendStatus(404);
      }

      response.status(200).json(cinema);
    });
  }
};