var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var MovieSchema = new Schema({
  name: { type: String, default: '' },
  cinemaId: { type: Schema.Types.ObjectId, ref: 'Cinema', default: null}
});

mongoose.model('Movie', MovieSchema);