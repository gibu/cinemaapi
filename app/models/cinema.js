var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var CinemaSchema = new Schema({
  name: { type: String, default: '' },
  suburb: { type: String, default: '' }
});

mongoose.model('Cinema', CinemaSchema);