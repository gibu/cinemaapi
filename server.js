var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var config = require('./config/config.json');

// Mongo
var connectToMongo = function () {
  var options = { server: { socketOptions: { keepAlive: 1 } } };
  mongoose.connect(config.mongo, options);
};
connectToMongo();
mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connectToMongo);
// end mongo

//models
require('./app/models/index');

var app = express();
app.use(bodyParser.json());
require('./routes')(app);

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
